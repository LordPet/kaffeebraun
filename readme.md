# RUBY PROJECT

## Server side:
* Ruby

## customer side:
* html
* sass
* jQuery
* angular.js
## DB:
* MySql
* MongoBD

## Description of the project:

It's a online shop for coffee and tea customer.

## features:

* login/account
* list of coffee and tea
* shopping cart
* items


# DB:

### Account:
- id
-  first name
-  last name
-  address
-  email
-  phone number
-  cellphone
-  province

### Drinks:
- id
- name
- pays
- color
- qte in stock
- type

### Command:
- id_account
- id_coffee
- quantite
- milling (mouture)

# Ruby + Rails setup (Ruby -v 2.2.1p85)
------------------
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
sudo apt-get install curl
\curl -sSL https://get.rvm.io | bash -s stable --rails
source ~/.rvm/scripts/rvm
rvm pkg install zlib
sudo gem install rails


*_to setup the 2.0.0 version:
 source ~/.rvm/scripts/rvm_*


# Coding standards
## General
1. Indent is equal to 4 spaces
2. Keep lines fewer than 80 characters
3. Never leave trailing whitespace
4. Everything in ENGLISH

## Methods
1. Methods shouldn't be more than 15 lines of code
2. Always comment your methods and keep a log of the last update made

## Ruby standard
1. Use spaces around operators, after commas, colons and semicolons, around { and before }
2. Put { at the end of a method definition
3. No spaces after (, [ or before ], )
4. No spaces after !
5. Use def with parentheses when there are arguments. Omit the parentheses when the method doesn't accept any arguments
6. Never use for, unless you know exactly why. Most of the time iterators should be used instead. for is implemented in
7. terms of each (so you're adding a level of indirection), but with a twist - for doesn't introduce a new scope (unlike each)
      and variables defined in its block will be visible outside it
8. Never use then for multi-line if/unless
9. The and and or keywords are banned. It's just not worth it. Always use && and || instead

## Nomenclature
1. Use snake_case for methods and variables
2. Use CamelCase for classes and modules. (Keep acronyms like HTTP, RFC, XML uppercase.)
3. Use SCREAMING_SNAKE_CASE for other constants

## Git
    1. Comment in ENGLISH
    2. Add new file : +
    3. Update a file : *
    4. Remove a file : -
    5. Comment a file : /

    - Follow your :heart:

    Based on : https://github.com/styleguide/ruby
